from flask import Blueprint
from flask import jsonify
from flask import request

mod = Blueprint('api', __name__)

sample_data = [
        {'author':'Ali', 'post':'Lorem ipsum'},
        {'author':'Ruth', 'post':'Dolor sit amet'},
        {'author':'Andrew', 'post':'Consectetur Adipiscing Elit'},
    ]

list_items = [
    {'item_name':'laptop','quantity':'1','price':20},
    {'item_name':'bag','quantity':'1','price':10},
    {'item_name':'tablet','quantity':'1','price':15},
]

cart_items = []

@mod.route('/getsampledata', methods=['GET','POST'])
def get_sample_data():
    print request.form.keys()
    if request.method=='POST':
        sample_data.append({
            'author':request.form['author'], 
            'post':request.form['post'], 
        })
    return jsonify(posts=sample_data)

@mod.route('/get-items', methods=['GET','POST'])
def get_items():
    if request.method == 'POST':
        list_items.append({
            'item_name': request.form['item_name'],
            'price' : request.form['price'],
            'quantity': 1,
        })
    return jsonify(items=list_items)

@mod.route("/get-cart-items", methods=['GET','POST'])
def get_cart_items():
    subtotal = 0
    to_remove = 0
    item = ""
    price = 0
    for x in xrange(0,len(cart_items)):
            subtotal += int(cart_items[x]["subtotal"])
    if request.method == 'POST':
        for x in xrange(0,len(list_items)):
            if list_items[x]["item_name"] == request.form["item_name"]:
                price = int(list_items[x]["price"])
                item = list_items[x]["item_name"]
                to_remove = x
        
        list_items.pop(to_remove)
        cart_items.append({
            'item_name': item,
            "quantity": int(request.form["item_quantity"]),
            'subtotal': price * int(request.form["item_quantity"]),
        })
        """
        for x in xrange(0,len(cart_items)):
            subtotal += int(cart_items[x]["subtotal"])
        """
        subtotal += int(cart_items[len(cart_items)-1]["subtotal"])
        """
        print "Cart Items:"
        print cart_items
        print "List Items:"
        print list_items
        print "Subtotal:"
        print subtotal
        """
    return jsonify(cart_items=cart_items, cart_subtotal=subtotal, list_items=list_items)

