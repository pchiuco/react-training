var Item = React.createClass({
  addToCart: function(e){
        e.preventDefault();
        var form = e.target;
        var item_quantity = parseInt(form.item_quantity.value.trim());
        var item_name = form.item_name.value.trim();
        if(!item_quantity)
        {
            return;
        }
        this.props.onAddToCart({item_name: item_name, item_quantity: item_quantity});
        form.item_quantity.value = '';
        return;
  },    
  render: function(){
    return (
        <tr>
            <td>{this.props.item_name}</td>
            <td>{this.props.price}</td>
            <td>
                <form onSubmit={this.addToCart} action="POST">
                    <input type="text" placeholder="quantity" name="item_quantity"/>
                    <input type="hidden" value={this.props.item_name} name="item_name"/>
                    <input type="submit" value="Add To Cart" className="btn btn-primary"/>
                </form>
            </td>
        </tr>
    );
  }
});

var CartItem = React.createClass({
    render: function(){
        return (
            <tr>
                <td>{this.props.item_name}</td>
                <td>{this.props.quantity}</td>
                <td>{this.props.subtotal}</td>
            </tr>
        );
    }
});

/*
var ItemList = React.createClass({
  
  render: function(){
    var itemNodes = this.props.data.map(function (item_data, index){
        return(
            <Item onAddToCart={this.handleAddToCart} item_name={item_data.item_name} price={item_data.price} quantity={item_data.quantity}/>//</Item>
        );
    }.bind(this)
    );
    return(
        <table className="table">
            <th>Item Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th></th>
            {{ itemNodes }}
        </table>
    );
  }
});
*/

/*var CartList = React.createClass({
   render: function(){
        var cartItemNodes = this.props.data.map(function(cart_item_data, index){
            return (
                <CartItem item_name={cart_item_data.item_name} quantity={cart_item_data.quantity} subtotal={cart_item_data.subtotal}></CartItem>
            );
        });
        return(
            <table className="table">
                <tr>
                    <th>Item Name</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
                {{ cartItemNodes }}
                <tr>
                <td>Total: </td>
                <td></td>
                <td>{this.props.cart_subtotal}</td>
                </tr>
            </table>
        );
   }
});*/

var AddToListForm = React.createClass({
    handleSubmit: function(e)
    {
        e.preventDefault();
        var form = e.target;
        var item_name = form.item_name.value.trim();
        var item_price = parseInt(form.price.value.trim());
        if(!item_name || !item_price)
        {
            return;
        }
        this.props.onItemSubmit({item_name: item_name, price: item_price});
        form.item_name.value = '';
        form.price.value = '';
        return;
    },
    render: function(){
        return(
            <form onSubmit={this.handleSubmit}>
                <input type="text" placeholder="Item name" name="item_name" className="form-control"/>
                <input type="text" className="form-control" placeholder="Price" name="price"/>
                <input type="submit" className="btn btn-primary" value="Add to List" />
            </form>
        );
    }
});

//Add To Cart Form
var InventoryBox = React.createClass({
    //load list items
    loadItemsFromServer: function(){
        $.ajax({
            url: this.props.list_url,
            dataType: 'json',
            cache: false,
            success: function(data){
                var itemDict = JSON.parse(JSON.stringify(data));
                this.setState({list: itemDict['items']});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });    
    },
    //load cart items
    loadCartItemsFromServer: function(){
        $.ajax({
            url: this.props.cart_url,
            dataType: 'json',
            cache: false,
            success: function(data){
                var itemDict = JSON.parse(JSON.stringify(data));
                this.setState({cart: itemDict['cart_items'],cart_subtotal:itemDict['cart_subtotal']});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });    
    },
    //AddToList form
    handleItemSubmit: function(item_data){
        $.ajax({
            url: this.props.list_url,
            dataType: 'json',
            type: 'POST',
            data: item_data,
            success: function(data){
                var itemDict = JSON.parse(JSON.stringify(data));   
                this.setState({list: itemDict['items']});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    //Add item to cart
    handleAddToCart: function(item_data){
        $.ajax({
            url: this.props.cart_url,
            dataType: 'json',
            type: 'POST',
            data: item_data,
            success: function(data){
                var itemDict = JSON.parse(JSON.stringify(data));  

                this.setState({cart: itemDict['cart_items']});
                this.setState({cart_subtotal: parseInt(itemDict['cart_subtotal'])});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.cart_url, status, err.toString());
            }.bind(this)
        });
        
   },
   //initial state
    getInitialState: function(){
        return {list: [], cart: [], cart_subtotal: 0};
    },
    //on mount
    componentDidMount: function(){
        this.loadItemsFromServer();
        setInterval(this.loadItemsFromServer, this.props.checkInterval);
        this.loadCartItemsFromServer();
        setInterval(this.loadCartItemsFromServer, this.props.checkInterval);
    },
    render: function(){
        var itemNodes = this.state.list.map(function (item_data, index){
            return(
                <Item key={index} onAddToCart={this.handleAddToCart} item_name={item_data.item_name} price={item_data.price} quantity={item_data.quantity}/>//</Item>
            );
            }.bind(this)
        );
        
        var cartItemNodes = this.state.cart.map(function(cart_data, index){
            return(
                <CartItem key={index} item_name={cart_data.item_name} quantity={cart_data.quantity} subtotal={cart_data.subtotal}></CartItem>
            );
        });

        var subtotal = this.state.cart_subtotal;
        console.log(subtotal);
        return(
                <div>
                <div>  
                    <table className="table">
                        <thead>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th></th>
                        </thead>
                        <tbody>
                            { itemNodes }
                        </tbody>
                    </table>
                </div>
                <div>
                    <AddToListForm onItemSubmit={this.handleItemSubmit}/>
                </div>
                <div>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Item Name</th>
                                <th>Quantity</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            { cartItemNodes}
                            <tr>
                                <td>Total:</td>
                                <td></td>
                                <td>{this.state.cart_subtotal}</td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
});

// Cart Box
/*
var CartBox = React.createClass({
    loadCartItemsFromServer: function(){
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data){
                var itemDict = JSON.parse(JSON.stringify(data));
                
                this.setState({data: itemDict['cart_items'], subtotal: itemDict["cart_subtotal"]});
            }.bind(this),
            error: function(xhr, status, err){
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });    
    },
    getInitialState: function(){
        return {data: [], subtotal: 0};
    },
    componentDidMount: function(){
        this.loadCartItemsFromServer();
        setInterval(this.loadCartItemsFromServer, this.props.checkInterval);
    },
    render: function(){
        return (
            <div>
                <CartList data={this.state.data} subtotal={this.state.cart_subtotal}/>
            </div>
        );
    }
});
*/