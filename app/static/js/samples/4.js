var Post = React.createClass({
  render: function() {
    var style = {
      backgroundColor: this.props.bgcolor
    }
    return (
      <div style={style}>
        {this.props.author}: {this.props.post}
      </div>
    );
  }
});

var PostListTwo = React.createClass({
  render: function() {
    return (
      <div>
        <Post bgcolor="#FF6600" author="Ali" post="Hello!"/>
        <Post bgcolor="#99CCFF" author="Jeru" post="And again, another greeting!"/>
      </div>
    );
  }
});